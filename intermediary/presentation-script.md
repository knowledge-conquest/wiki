# Knowledge Conquest - Presentation

[TOC]

## Introduction (2 min)

- What is KC

  Bonjour a toutes et a tous, je vais vous presenter Knowledge Conquest. Ce jeu existe dans le cadre de mon travail de bachelor. La phase actuelle consiste grandement a la creation d'un prototype et a la mise en place du projet.

  Je vais vous montrer l'avancement actuel en commençant par le choix de l'architecture et des technologies utilisées. On verra quelques implementation notables et terminerons par un petite demonstration du prototype.

  Knowledge Conquest est un jeu de role incremental infini ou Endless RPG/Idler pour les connaisseur. 

  Pour ceux qui ne savent pas ce qu'est un jeu Idler, c'est un jeu incremental ou ,traduit literalement, inactif. Le but de ce type de jeu est souvent de gagner de plus en plus de points. Sous forme d'argent, de resources ou de niveau. Le joueur effectue de simples actions, tels que des clics, ou laisse simplement le jeu tourner tout seul.

  Pour ce qui est de Knowledge Conquest, le joueur se trouve dans une region d'un monde infini. Il voyage a travers la region, de zone en zone pour s'entrainer, recolter des resources et combatre des monstrers. Tout au long de sont aventure il acumule de plus en plus de connaissance comme la maitrise d'une certaine arme ou diffentes competences. Une fois une region complètement conquise, le joueur passe aux regions suivante grace aux zones de teleportation. Les regions deviennent de plus en plus compliquées jusqu'a ce que les connaissance actuelles de l'aventurier ne suffisent plus. Après une mort definitive du a un monstre trop fort par exemple ou as un moment voulu, le joueur se réincarne dans un nouveau monde. Il perd tout ce qui possédait, mais garde ces connaissance pour recommencer plus rapidement et en acquérir de nouvelles.

  ```
  Introduction fun.
  Pour ce qui est de Knowledge Conquest, le but est de gagner un maximum de connaissance en parcourant un monde infini. Imaginons Steve! Un maitres des art martiaux de niveau 1. Il aimerais devenir plus fort mais n'y connait rien. Il part en quete de pouvoir. En parcourant le monde il ammasse plein de resources, pierres, bois, fer, etc. En voyant les monstres au tour de lui il décide de s'équiper mais ne sait pas quoi faire de sont arme. C'est l'heure de s'entrainer. Un peu de meditation et d'escrime et hop niveau 2. Quelques monstres plus tard. Niveau 5! Enfin pret pour la suite. en se rendant a la zone de teleportation la plus proche il part dans le region suivante. bien evidement les monstre sont plus forts ici. mais steve est courageux et part a la chasse. En se retrouvant nez a nez avec un loup de niveau 10, il fini en nouriture en un rien de temps. Maintenant, Steve, niveau 1, nouvelle zone inconnue. Il n'a plus rien sur lui. Mais se souvient de son entrainement. Il repart lors en quete de vendeance.
  ```

  

## Architecture (2 min)

- Microservices
- Auth service
- Adventurer service
- Potential separation
- Web app

L'architecture. le project est base sur une architecture dite Microservices. Il s'agit d'un alternative a un architectique monolithiqu. Dans une architecture Microservice, l'application est explosee en plusieurs petits microservices.

Un microservice fait partie d'une grande application et peut etre plus ou moins grand. Il se veux le plus independant possible. Chaque service a un role bien défini.

show diagram and explain.

## Technologies (1 min)

- Play framework
- Mysql
- Arango DB
- Nodejs, Vuejs, Tailwindcss
- Docker
- Kubernetes

Voici les différentes technologies utilise ou qui vont etre utilisées. 

Tout d'abord, Play est un framework java et scala permettant de creer des application web. Dans le cadre de se projet il est utilise pour créer les microservices principaux sous forme d'APIs REST.

Ensuite, deux types de base de donnes sont actuellement utilise. MySQL sert pour le service d'authentication alors que ArangoDB est utilisées pour l'Adventurer service.

L'interface utilisateur est creer avec Nodejs, vuejs et tailwindcss.

Tous les services sont dockerize via le pipeline CI/CD de Gitlab. Par la suite j'aimerais utiliser Kubernetes pour gere toute l'infrastructure et le deploiement de l'application.

Ici on peut bien voir un des avantages de l'architecture Microservices. Elle permet d'utilise n'importe qu'elles technologies pour chaque microservice. Certaines technologies peuvent etre plus efficaces que d'autres dans certains cas.

## Object Document Mapping (5 min)

- Conceptual data model
- Game models
- Document, Reference and Edge classes
- Jackson
- How it works

## Realm Generation (5 min)

- Realms, regions and areas
- Maze generation
- Region generation
- Improvements
- Generation in general

## Application Demo (3 min)

- Application showcase
- Show login, register and adventurer selection
- Adventurer profile
- Travel and teleportation
- Fight

## Conclusion (2 min)

- Current features and next features
- Timeline
- ending word

