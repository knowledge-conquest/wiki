# Knowledge Conquest

Knowledge Conquest is an endless RPG/Idler taking place in a world ruled by martial arts. The player is part of a mystic race, which has one special ability. Reincarnation. The player lives through multiple lives to gain a maximum of knowledge. The goal is to become more and more powerful.

The project primarily consists of creating a game server where users can play in their realms and execute actions (gathering, crafting, training, etc.) or fight enemies. A web application will be used by players to access the game server.

## Features

Players travel through their own realms. A new randomly generated realm is created after each reincarnation. Adventurers visit the various areas of their realms to become more powerful.

Martial artists gain skills, cultivation methods and weapon proficiencies as knowledge through each reincarnation. On death everything is lost, but the knowledge acquired.

Through passive or active actions, players gain not only knowledge but also various items and recipes to temporary increase their power.

Enemies might be in the way of success. Players use their knowledge and equipments to fight enemies during automatic battles agains small monsters or epic turn-based battles agains bosses.

## Technologies

The game server will use the micro-service architecture. Each micro-service have a specific role. The Java Play Framework will be used to create RESTful APIs or Websocket services. Services might have a database, such as MySQL or ArangoDB depending on the needs.

- Authentication Service: Manage users information and credentials. Provide Json Web Tokens.
- Martial Artist Service:  Controls the information of a martial artist, such as knowledge, level or skills.
- Realm Service: Store realm data and manage actions. Communicates with the Martial Artist Service to update their data whenever an action is executed.
- Combat Service: Manages turn-based combats. Combats are created by the Realm Service. The player connects to the combat service for the fights. The results are sent back to the Realm Service.
- Generation Service: Used by the other services to create random data, such as realms, weapons or skills.

In order to organise those services, Kubernetes will be used to set up the infrastructure.

The web application will be created with NodeJs and VueJs as a thin client. The application will serve as UI and contact the various services for all the game logic and data.

